#!/usr/bin/env bash

rm -f *.avs
rm -f flow*.mat_head flow*.vec_head flow*.sca_head flow*.chk flow*.fin flow*.outp flow*.avs_log simu_thickness_* simu_thickness.* thickness
rm -f trans.mat_head trans.vec_head trans.sca_head trans.chk trans.fin trans.outp trans.avs_log
rm -f fehmn.err boun.macro boun_st.macro
./write_boun.x
/turquoise/users/zhiming/bin/xfehm flow0.files
/turquoise/users/zhiming/bin/xfehm flow.files
/turquoise/users/zhiming/bin/xfehm trans.files
./post_process_avs_getpwz.x
cat ./simu_thickness.dat | awk '{print $3}' > thickness
./post_process_conc.x
