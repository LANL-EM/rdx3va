template |
trac
userc  0    1    1.00E-02   1.0
 0  365250     365250   0 #G2: DAYCS, DAYCF, DAYHF, DAYHS
 8  1.8     1.00E-01     1.00E+06   100000
 2  # G6: NSPECI=number of species
 1  rdx  # G11: ICNS, 0= solid, 1 = liquid
1 |  kd_10_Tpf2 | 0 1 0 |  diffusion   |  |   lx  | |  ly  |  |   lz   |
1 |  kd_11_Tvt2 | 0 1 0 |  diffusion   |  |   lx  | |  ly  |  |   lz   |
1 |  kd_14_Tpf3 | 0 1 0 |  diffusion   |  |   lx  | |  ly  |  |   lz   |
1 |  kd_16_Qbof | 0 1 0 |  diffusion   |  |   lx  | |  ly  |  |   lz   |
1 |  kd_17_Qct  | 0 1 0 |  diffusion   |  |   lx  | |  ly  |  |   lz   |
1 |  kd_19_Qbt1g | 0 1 0 |  diffusion   |  |   lx  | |  ly  |  |   lz   |
1 |  kd_21_Qbt1vu | 0 1 0 |  diffusion   |  |   lx  | |  ly  |  |   lz   |
1 |  kd_22_Qbt2 | 0 1 0 |  diffusion   |  |   lx  | |  ly  |  |   lz   |
1 |  kd_23_Qbt3 | 0 1 0 |  diffusion   |  |   lx  | |  ly  |  |   lz   |
1 |  kd_25_Qbt4 | 0 1 0 |  diffusion   |  |   lx  | |  ly  |  |   lz   |

-10    0    0    1
-11    0    0    2
-14    0    0    3
-16    0    0    4
-17    0    0    5
-19    0    0    6
-21    0    0    7
-22    0    0    8
-23    0    0    9
-24    0    0    9
-25    0    0   10 
-27    0    0    4 
-28    0    0    4 
-140   0    0    3
-141   0    0    3
-142   0    0    3

     1    0       0       1.00E-20

59680 59680  1      -9876   .0000  1e20

 1  rdx  # G11: ICNS, 0= solid, 1 = liquid
1 |  kd_10_Tpf2 | 0 1 0 |  diffusion   |  |   lx  | |  ly  |  |   lz   |
1 |  kd_11_Tvt2 | 0 1 0 |  diffusion   |  |   lx  | |  ly  |  |   lz   |
1 |  kd_14_Tpf3 | 0 1 0 |  diffusion   |  |   lx  | |  ly  |  |   lz   |
1 |  kd_16_Qbof | 0 1 0 |  diffusion   |  |   lx  | |  ly  |  |   lz   |
1 |  kd_17_Qct  | 0 1 0 |  diffusion   |  |   lx  | |  ly  |  |   lz   |
1 |  kd_19_Qbt1g | 0 1 0 |  diffusion   |  |   lx  | |  ly  |  |   lz   |
1 |  kd_21_Qbt1vu | 0 1 0 |  diffusion   |  |   lx  | |  ly  |  |   lz   |
1 |  kd_22_Qbt2 | 0 1 0 |  diffusion   |  |   lx  | |  ly  |  |   lz   |
1 |  kd_23_Qbt3 | 0 1 0 |  diffusion   |  |   lx  | |  ly  |  |   lz   |
1 |  kd_25_Qbt4 | 0 1 0 |  diffusion   |  |   lx  | |  ly  |  |   lz   |

-10    0    0    1
-11    0    0    2
-14    0    0    3
-16    0    0    4
-17    0    0    5
-19    0    0    6
-21    0    0    7
-22    0    0    8
-23    0    0    9
-24    0    0    9
-25    0    0   10 
-27    0    0    4 
-28    0    0    4 
-140   0    0    3
-141   0    0    3
-142   0    0    3

     1    0       0       1.00E-20

-101  0   0      -9876   .0000  1e20


