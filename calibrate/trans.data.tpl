template |
** 3D UZ SZ model
cont
avs 500000 1e20
concentrations
xyz
endavs
rest
lflux
 
#nobr
airw
1
15.  0.1
rich
0.95 1.e-5 1e-4 1e-4
zone
file
../grid/tet_ev_material.zone
zonn
file
../grid/wtr_UPZ2_Tpf3s.zonn
zonn   # 141
file
../grid/wtr_UPZ2_Tpf3s_win.zonn
zonn   # 142
file
../grid/wtr_UPZ2_Tpf3s_intr.zonn
perm
file
./perm.dat
rock
file
../properties/rock.dat
rlp
file
../properties/rlp.dat
zonn
file   # zone 60
../grid/wtr_UPZ2_blw_addNW2.zonn
zonn
file   # zone 61
../grid/wtr_UPZ2_in_addNW2.zonn
zonn   # 141 re-defined, because they also in zone 61
file
../grid/wtr_UPZ2_Tpf3s_win.zonn
zonn   # 142
file
../grid/wtr_UPZ2_Tpf3s_intr.zonn
zonn
file   # zone 70
../grid/wtr_LPZ1_blw.zonn
zonn
file   # zone 71
../grid/wtr_LPZ1_in.zonn
itfc
60  61  |   itfc_6061   |
70  71  |   itfc_7071   |
141 142 |   itfc_141142   |
 
 
zone
file
../grid/wtr_abv.zone
pres
-40 0 0 0.1 0.1 2
 
pres
file  # saturated zone
../fehm/setBoundConditions_addNW/pres_sat.macro
pres
file  # upz2
../fehm/setBoundConditions_addNW2/pres_UPZ2.macro
pres
file  # LPZ1
../fehm/setBoundConditions/pres_lpz1.macro
# flow as boundary conditions on all lateral boundaries in the saturate zone
flow
file   # fixed head at saturated zone nodes
../fehm/setBoundConditions_addNW/flow_sat.macro
flow
file      #  constat head on all perched boudnary nodes
../fehm/setBoundConditions_addNW2/flow_UPZ2_allsides.macro
#
zone
file
../grid/wtr_blw.zone
zonn
file   # zone 4
../grid/out_south_front.zonn
zonn
file   # zone 6
../grid/out_north_back.zonn
zonn
file  # zone 7
../grid/out_north_east.zonn
zonn
file  # zone 3
../grid/out_west_left.zonn
zonn
file  # zone 5
../grid/out_east_right.zonn
zonn
file  # zone 1
../grid/out_top.zonn
zonn
file  # zone 102
../grid/canyon_bot.zonn
zonn
file  # zone 2
../grid/out_bottom.zonn
zonn
file  # zone 101. Note this will overwrite some nodes in out_bottom.zonn
../grid/canyon_cdv.zonn
zonn
file    # perched zones (13-17)
../fehm/setBoundConditions_addNW2/UPZ2_allsides.zonn
#
boun
file
./boun.macro 
flxz
15
1  2   3   4   5   6  7  9  13 14  15  16 17  101 102
time
file
../data/time.macro
ctrl
15 1e-9 1 850 bcgs
1 0 0 1
 
1.0 3 1.0
15 1.8 1e-25 1e20
0 1
sol
1 -1
iter
1.e-5 1.e-5 1.e-05 -5.e-5 1.1
0 0 0 0  1e10
# redefine zones for assigning transport parameters to different materials
zone
file
../grid/tet_ev_material.zone
zonn
file  # zone 101. Note this will overwrite some nodes in material zones
../grid/canyon_cdv.zonn
zonn
104
list
492650  537940  2200


trac
file
./trac.macro
stop
