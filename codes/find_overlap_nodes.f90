integer :: nodes(1000)

open(10,file='../grid/canyon_cdv.zonn')
read(10,*)
read(10,*)
read(10,*)
read(10,*) npoints
read(10,*) (nodes(i), i=1,npoints)
close(10)

open(11,file='obs_nodes.dat')
read(11,*) nobs
do i=1,nobs
   read(11,*) itmp
   do j=1,npoints
      if(itmp == nodes(j)) write(*,*) i, itmp
   enddo
enddo

end
