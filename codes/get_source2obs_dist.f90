implicit none
integer, allocatable, dimension(:) :: nodesA
real*8, allocatable, dimension(:,:) :: xy
real*8, allocatable, dimension(:) :: x, y, z
integer, allocatable, dimension(:,:) :: ncol
character(len =200) :: gfile, sourcefile,wellfile, surffile, tmpStr, str
integer :: nfile, zoneA, np, nnodeA, nxy, nnode, imatch 
integer :: i, j, itmp, jtmp, j0, j1, ind

real*8 :: x1, y1, x0, y0, z0, tmp, dist, dist_min, cutoff


open(11,file='get_source2obs_dist.input')
read(11,'(a)') gfile
read(11,'(a)') sourcefile
read(11,'(a)') wellfile
read(11,'(a)') surffile
read(11,*) cutoff
close(11)

! open grid file
open(12,file=gfile)
read(12,*)
read(12,*) nnode
allocate(x(nnode), y(nnode), z(nnode))
do i=1,nnode
   read(12,*) itmp, x(i), y(i), z(i)
enddo
close(12)

! open the zone file for nodes in the zone
open(13,file=sourcefile)
read(13,*) 
read(13,*) zoneA 
read(13,*) 
read(13,*) nnodeA
allocate(nodesA(nnodeA))
read(13,*) (nodesA(i), i=1,nnodeA)
write(*,*) 'number of nodes in the main zone', nnodeA
close(13)

open(21,file='source2obs_dist.dat')

open(14,file=wellfile)
read(14,*) np
do i=1,np
   read(14,*) tmpStr, x0, y0, z0
   dist_min = 1.0D20
   do j=1,nnodeA
      jtmp = nodesA(j)
      dist = sqrt((x(jtmp) - x0)**2 + (y(jtmp) - y0)**2 + (z(jtmp) - z0)**2)
      if(dist < dist_min) then
         dist_min = dist
         j0 = j
      endif
   enddo
   write(21,'(a20,3f15.5, i10, f15.5)') trim(tmpStr), x0, y0, z0, nodesA(j0), dist_min
enddo

write(21,*)

!  for the observation locations at the surface, we first need to find
!  the node corresponding to location, and then find the distance from this node 
!  to the source locations 

allocate(xy(2,nnode), ncol(nnode/10,0:200))
nxy = 0
do i=1,nnode   ! map all nodal points to xy plane
   x0 = x(i); y0=y(i) ! pick this point
   imatch =  0
   do j=1,nxy         ! go through saved points
      x1 = x(j); y1=y(j)
      if( x0 == x1 .and. y0 == y1) then
           imatch = 1
           itmp = ncol(j,0)
           itmp = itmp  + 1
           ncol(j,0) = itmp
           ncol(j,itmp) = i
      endif
   enddo
   if(imatch ==0) then   ! add this point to the list
     nxy = nxy + 1
     xy(1,nxy) = x0; xy(2,nxy) = y0
     ncol(nxy,0) = 1
     ncol(nxy,1) = i
   endif
enddo

!  now start to find the  distance between obs location and source 
  ind = 0
  open(14,file=surffile)
  read(14,*) np 
  do i=1,np                 ! loop over all obs locations
     read(14,*) tmpStr, x0, y0
     dist_min = 1.d20       ! find the column
     do j=1,nxy             ! loop over all mapped nodes to find out the closest in x and y
        itmp = ncol(j,1)    ! all ncol(j,2...n_ have the same xy value
        dist = dsqrt( (x(itmp) -x0)**2 + (y(itmp)-y0)**2) 
        if(dist < dist_min) then
           dist_min = dist
           j0 = j
        endif
     enddo

!   now if the dist_min < cutoff, calculate the distance, otherwise, just ignore this obs location 
    if(dist_min < cutoff ) then

!      then find the highest node in this column
       tmp = -1d20
       itmp = ncol(j0,0)     ! the total number of nodes in j0-th column
       do j=1,itmp           ! loop over all these nodes
          jtmp= ncol(j0,j)   ! pick a node
          if(z(jtmp) > tmp) then
              j1 = j         ! update the node number 
              tmp = z(jtmp)  ! update the high elevation
          endif
       enddo
       jtmp = ncol(j0,j1)   ! this is the node number closest to the obs location

!      now determine the distance from this node to source 
       z0 = z(jtmp)
       dist_min = 1.0D20
       do j=1,nnodeA
          jtmp = nodesA(j)
          dist = sqrt((x(jtmp) - x0)**2 + (y(jtmp) - y0)**2 + (z(jtmp) - z0)**2)
          if(dist < dist_min) then
             dist_min = dist
             j0 = j
          endif
       enddo

       write(21,'(a20,3f15.5, i10, f15.5)') trim(tmpStr), x0, y0, z0, nodesA(j0), dist_min
    endif

   enddo
close(21)

end


