implicit none
real*8, allocatable, dimension(:) :: x, y, z, z_wtr, thick
real*8, allocatable, dimension(:,:) :: xy
real*8, allocatable, dimension(:) :: simu_thick, x_perch1, y_perch1, sat
integer, allocatable, dimension(:) ::  nodes_pwz1
integer, allocatable, dimension(:,:) :: nodes_pwz
integer :: id_b(10), nnode_b(10), ind(10)
real*8 :: z_highest, z_lowest, x0, y0, z0, x1, y1,  minElevation(10), maxElevation(10)
integer :: i,j, itmp, jtmp, imatch, ipwz, k, npwz, nline, nsize, num, nxy, node_lowest, node_highest
integer :: nnode_pwz, nnode_pwz0(10), nnode, iunit
real*8 :: tmp
character(len=200):: avsfile, tmpStr

open(11,file='post_process_avs_getpwz.input')
read(11,'(a)') avsfile
read(11,*) npwz
do i=1,npwz
   read(11,*) minElevation(i), maxElevation(i)
enddo
close(11)

open(12,file=avsfile)
read(12,*) nline 
do i=1,nline     ! skip header
   read(12,*)
enddo
nnode = 0
101  read(12,*,end=102) num
     nnode = nnode + 1
     goto 101
102  write(*,*) nnode
rewind(12)
read(12,*) nline 
do i=1,nline     ! skip header
   read(12,*)
enddo

allocate(x(nnode), y(nnode), z(nnode), sat(nnode), nodes_pwz(10,nnode))
ind = 0
do i=1,nnode
   if(nline ==6) then
      read(12,*) itmp, x(i), y(i), z(i), tmp, tmp, sat(i)
   else
      read(12,*) itmp, x(i), y(i), z(i), tmp, sat(i)
   endif
   do j=1,npwz
      if(z(i) > minElevation(j) .and. z(i) < maxElevation(j) .and. sat(i) > 0.99) then
         ind(j) = ind(j) + 1
         nodes_pwz(j,ind(j)) = i   ! all saturated nodes for different pwz
      endif
   enddo
enddo
close(12)
nnode_pwz0 = ind     ! nnode_pwz0 is array storing the number of saturated nodes
write(*,*) 'the number of saturated nodes in pwz =', nnode_pwz0(1:npwz)

! project the mesh onto xy plane
allocate(xy(2,nnode))
nxy = 0
do i=1,nnode
   x0 = x(i); y0=y(i) ! pick this point
   imatch =  0
   do j=1,nxy         ! go through save points
      x1 = x(j); y1=y(j)
      if( x0 == x1 .and. y0 == y1) imatch = 1
   enddo
   if(imatch ==0) then   ! add this point to the list
     nxy = nxy + 1
     xy(1,nxy) = x0; xy(2,nxy) = y0
   endif
enddo

! for each node in this list, find the highest node from all perched nodes that are above this node
! The highest location will be used to determine the hydraostatic head on these nodes.

  open(31,file='simu_thickness.dat')
do ipwz=1,npwz      ! loop for all pwz's
   tmpStr ='simu_thickness_x_plot.dat' 
   write(tmpStr(16:16),'(i1.1)') ipwz
   iunit = 40+ipwz
   open(iunit,file=tmpStr)
   nnode_pwz = nnode_pwz0(ipwz)  ! number of saturated nodes in zone ipwz
   allocate(z_wtr(nnode_pwz), simu_thick(nnode_pwz))
   z_wtr = 0.0d0;  simu_thick = 0.0d0
   do i=1,nnode_pwz
      itmp = nodes_pwz(ipwz,i)  ! node number
      x0 = x(itmp); y0=y(itmp); z0 = z(itmp)
      z_highest = -1.0d20
      z_lowest = 1.0d20
      do j=1,nnode_pwz
         jtmp = nodes_pwz(ipwz,j)
         if(x(jtmp) == x0 .and. y(jtmp) == y0) then
            if(z(jtmp) > z_highest) then
              z_highest =  z(jtmp)
              node_highest = jtmp
            endif
            if(z(jtmp) < z_lowest) then
              z_lowest =  z(jtmp)
              node_lowest = jtmp
            endif
         endif
      enddo
      simu_thick(i) = z(node_highest) - z(node_lowest)
      z_wtr(i) = z_highest
   enddo

! now array thickness includes all nodes in the perched zone.
! check these nodes and keep one thickness value for any pair of (x,y), in other words, 
! For any column of perched zone nodes, keep one thickness value.
   allocate(x_perch1(nnode_pwz), y_perch1(nnode_pwz), thick(nnode_pwz), nodes_pwz1(nnode_pwz))
   k = 1
   itmp = nodes_pwz(ipwz,1)  ! node number
   x_perch1(1) = x(itmp); y_perch1(1) = y(itmp) 
   thick(1) = simu_thick(1)
   nodes_pwz1(1) = itmp
   outer: do i=2,nnode_pwz
             itmp = nodes_pwz(ipwz,i)  ! node number
             x0 = x(itmp); y0=y(itmp)
             do j=1,k
                jtmp = nodes_pwz1(j)
                if(x(jtmp) == x0 .and. y(jtmp) == y0) then
                  ! found a match so start looking again
                  cycle outer
                endif
            enddo
            ! no match found, so add it to the output
            k = k + 1
            x_perch1(k) = x0; y_perch1(k) = y0
            thick(k) = simu_thick(i)
            nodes_pwz1(k) = itmp
          enddo  outer
    nsize = k

! now write the thickness array for all xy locations
  do i=1,nxy
     x0 = xy(1,i); y0=xy(2,i)
     imatch = 0
     do j=1,nsize
        if( abs(x0-x_perch1(j)) < 0.1 .and. abs(y0-y_perch1(j)) < 0.1 ) then
           imatch = 1
           tmp = thick(j)
           goto 103
        endif
     enddo
103  if(imatch .ne. 1) tmp = 0.0d0
     write(31,'(3f15.5)') x0, y0,tmp
     write(iunit,'(3f15.5)') x0, y0,tmp
  enddo
  deallocate(z_wtr, simu_thick,x_perch1,y_perch1, thick, nodes_pwz1)
  close(iunit)
enddo

end


