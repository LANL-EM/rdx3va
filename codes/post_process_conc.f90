       integer, allocatable, dimension(:) :: obsNodes
       real*8, allocatable, dimension(:,:) :: conc
       character(len=100) :: fnames(500), str
       real*8 :: simt(1000), obst(50,1000), obs_conc(1000), tmp
       character*10, parameter:: list_conc='list_conc'


open(10,file='../grid/tet.fehmn')
read(10,*)
read(10,*) nnode

!  read trans.trc for obs locations
       open(11,file='obs_nodes.dat')
       read(11,*) npoints
       allocate(obsNodes(npoints))
       do i=1,npoints
          read(11,*) obsNodes(i)
       enddo
       close(11)

! read trans.avs_log
       open(12,file='trans.avs_log')
       read(12,*)
       read(12,*)
       read(12,*)
       read(12,*)
       nfile = 0
100    read(12,*, end=101) str, t0
       nfile = nfile + 1
       fnames(nfile) = str
       simt(nfile) = t0 
       goto 100
101  close(12)


! read conc files
     allocate(conc(nfile,nnode))
     do i=1,nfile
        str=trim(fnames(i))//'_con_node.avs'
!        write(*,*) str
        open(15,file=str)
        read(15,*) nvar
        do j=1,nvar   ! skip lines
           read(15,*) 
        enddo
        do j=1,nnode
           read(15,*) itmp, tmp, tmp, tmp, tmp1, tmp2
           conc(i,itmp) = tmp1 + tmp2
        enddo
        close(15)
     enddo
! the unit of conc from FEHM output files is kg/mol
! need to convert to ppb
     conc = conc/4.50207d-09

open(11,file='obs_time_conc.dat')
read(11,*) npoints
nobs = 0
do i=1,npoints
   read(11,*) str, nt
   do j=1,nt
      read(11,*) t, conc0
      nobs = nobs + 1
      obs_conc(nobs) = conc0
   enddo
enddo
close(11)

open(11,file='obs_time_conc.dat')
read(11,*) npoints
open(21,file='sim_conc.dat')
open(22,file='comp_conc.dat')
ind =0
tmp = 0.0d0
do i=1,npoints
   read(11,*) str, nt, inode, w
   if( w > 0.0) write(22,*) str
   do j=1,nt
      read(11,*) t, conc0
      do k=1,nfile
         if( abs(simt(k) - t) < 0.1) then
            ind =  ind + 1
            write(21,*) conc(k,inode)
            tmp = tmp + w**2*(conc(k,inode) - obs_conc(ind))**2
            if(w > 0.0) write(22,*) t, obs_conc(ind), conc(k,inode)
            goto 301
         endif
      enddo
301  continue
   enddo
enddo
write(*,*) 'mean squared error = ', tmp/ind
close(11)



end
