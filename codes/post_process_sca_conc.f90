implicit none
       integer, allocatable, dimension(:) :: obsNodes
       real*8, allocatable, dimension(:,:) :: conc, sat
       real*8, allocatable, dimension(:) :: vol, por
       character(len=100) :: fnames(500), str
       real*8 :: simt(1000), obst(50,1000), obs_conc(1000)
       real*8 :: tmp, tmp1, tmp2, tmps, tmpc, tmpa(20)
       real*8 :: uzmass, szmass, t0
integer :: i,j,ind, itmp, nnode, nvar, nfile
       character*10, parameter:: list_conc='list_conc'

open(10,file='../grid/tet.fehmn')
read(10,*)
read(10,*) nnode
close(10)

! read volumn for each node
allocate(vol(nnode))
open(10,file='../grid/tet.stor')
read(10,*)
read(10,*)
read(10,*)
read(10,*) (vol(i),i=1,nnode)
close(10)

!  read porosity
allocate(por(nnode))
open(11,file='flow.mat_node.avs')
read(11,*) nvar
ind = 0
do i=1,nvar + 1
   read(11,*)str
   write(*,*) trim(str)
   if(str(1:3) .eq. 'Por') ind = i
enddo
write(*,*) ind

do i=1,nnode
   read(11,*) (tmpa(j),j=1,ind), por(i)
!   write(90,*) por(i)
enddo

! read trans.avs_log
       open(12,file='trans.avs_log')
       read(12,*)
       read(12,*)
       read(12,*)
       read(12,*)
       nfile = 0
100    read(12,*, end=101) str, t0
       nfile = nfile + 1
       fnames(nfile) = str
       simt(nfile) = t0 
       goto 100
101  close(12)


! read conc files
!     allocate(conc(nfile,nnode),sat(nfile,nnode))
     open(21,file='mass_distribution.dat')
     do i=1,nfile
        uzmass = 0.0; szmass = 0.0
        str=trim(fnames(i))//'_con_node.avs'
        open(15,file=str)

        str=trim(fnames(i))//'_sca_node.avs'
        open(16,file=str)

        read(15,*) nvar
        do j=1,nvar   ! skip lines
           read(15,*) 
        enddo
        read(16,*) nvar
        do j=1,nvar   ! skip lines
           read(16,*) 
        enddo

        do j=1,nnode
           read(15,*) itmp, tmp, tmp, tmp, tmp1, tmp2
           tmpc = tmp1 + tmp2

           read(16,*) itmp, tmp, tmp, tmp, tmps
           write(32,*) j, itmp, tmps, tmpc, por(j), vol(j)
           if(tmpc > 2e-19) then
              write(33,*) i, j, tmps, tmpc, por(j), vol(j)
              if(tmps <1.0d0) then
                 uzmass = uzmass + vol(j)*por(j)*tmps*997.80831*tmpc
               else
                 szmass = szmass + vol(j)*por(j)*997.80831*tmpc
               endif
            endif
        enddo
        close(15)
        close(16)
        write(21,'(f12.2, 3f15.4)') simt(i), uzmass, szmass, uzmass + szmass
     enddo
! the unit of conc from FEHM output files is moles/kg
! need to convert to ppb
!     conc = conc/4.50207d-09


end
