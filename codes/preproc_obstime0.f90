real*8 :: time0(1000), conc(1000), w(1000)
character(len=100)::  str, str1

open(11,file='obs_time_conc.dat')
read(11,*) npoints
open(24,file='obs_nodes.dat')
write(24,*) npoints
nobs = 0
do i=1,npoints
   read(11,*) str, nt, itmp, tmp0
   write(24,*) itmp
   write(*,*) str, nt, i
   do j=1,nt
      read(11,*) t, conc0
      write(*,*) 'here', t, conc0, j
      nobs = nobs + 1
      time0(nobs) = t
      conc(nobs) = conc0
      w(nobs) = tmp0
   enddo
enddo
close(11)
close(24)


! write entries or *.mads target
open(22,file='for_mads')
open(23,file='sim_conc.inst')
do i=1,nobs
   if(i <10) then
      str='T-x'
      write(str(3:3), '(i1.1)') i
      str1='!T-x!'
      write(str1(4:4), '(i1.1)') i
   elseif(i<100) then
      str='T-xx'
      write(str(3:4), '(i2.2)') i
      str1='!T-xx!'
      write(str1(4:5), '(i2.2)') i
   elseif(i<1000) then
      str='T-xxx'
      write(str(3:5), '(i3.3)') i
      str1='!T-xxx!'
      write(str1(4:6), '(i3.3)') i
   elseif(i<10000) then
      str='T-xxxx'
      write(str(3:6), '(i4.4)') i
      str1='!T-xxxx!'
      write(str1(4:7), '(i4.4)') i
   endif
   write(22,'(a1, a6, a10, f12.2, a12, f8.2, a2)' )'-',trim(str),': {target:', conc(i), &
        ', weight: ', w(i),'}'  
   write(23,'(a7)') str1
enddo

end
