implicit none
integer :: ndata, i
real*8 :: infil, canyon_flux, west_flux, cdv_flux, factor
real*8 :: outfall260(70)
integer ::time(70)

open(11, file='boun.para')

read(11,*) infil
read(11,*) canyon_flux
read(11,*) west_flux
read(11,*) cdv_flux
close(11)
open(12,file='outfall260.dat')
read(12,*) ndata, factor
if(factor > 1.0 .or. factor < 0.0) then
  write(*,*) 'check factor value'
  stop
endif
read(12,*) (time(i), i=1,ndata)
read(12,*) (outfall260(i), i=1,ndata)
close(12)


open(21,file='boun.macro')
write(21,'(a4)') 'boun'
write(21,'(a)') 'model 1  #background infiltration at top'
write(21,'(a2)') 'ti'
write(21,'(a)') '2 0 1e20'
write(21,'(a4)') 'wgtz'
write(21,'(a3)') 'dsw'
write(21,'(2e12.4)') infil, infil

write(21,'(a)') 'model 2  #canyon bottom, zone 102'
write(21,'(a2)') 'ti'
write(21,'(a)') '2 0 1e20'
write(21,'(a4)') 'wgtz'
write(21,'(a3)') 'dsw'
write(21,'(2e12.4)') canyon_flux, canyon_flux

write(21,'(a)') 'model 3   #kz = 0 at the bottom'
write(21,'(a2)') 'ti'
write(21,'(a)') '2 0 1e20'
write(21,'(a2)') 'kz'
write(21,'(2f8.2)') -20.0, -20.0

write(21,'(a)') 'model 4  # flux on west pw zone boundary nodes'
write(21,'(a2)') 'ti'
write(21,'(a)') '2 0 1e20'
write(21,'(a3)') 'dsw'
write(21,'(2e12.4)') west_flux, west_flux


write(21,'(a)') 'model 5   #water input at outfall 260 (node 5698)'
write(21,'(a2)') 'ti'
write(21,'(i4)')  ndata
write(21,'(70I8)') (time(i),i=1,ndata)
write(21,'(a3)') 'dsw'
write(21,'(70e14.5)') (-factor*outfall260(i),i=1,ndata)

write(21,'(a)') 'model 6   #water input in cdv canyon from outfall 260)'
write(21,'(a2)') 'ti'
write(21,'(i4)')  ndata
write(21,'(70I8)') (time(i),i=1,ndata)
write(21,'(a3)') 'dsw'
write(21,'(70e14.5)') (-(1.0-factor)*outfall260(i)+cdv_flux,i=1,ndata)
write(21,'(a3)') 'end'

write(21,'(a)') '-1    0   0  1'
write(21,'(a)') '-102  0   0  2'
write(21,'(a)') '-2    0   0  3'
write(21,'(a)') '-13   0   0  4'
write(21,'(a)') '5698 5698 1  5'
write(21,'(a)') '-101  0   0  6'
write(21,*)
close(21)

open(21,file='boun_st.macro')
write(21,'(a4)') 'boun'
write(21,'(a)') 'model 1  #background infiltration at top'
write(21,'(a2)') 'ti'
write(21,'(a)') '2 0 1e20'
write(21,'(a4)') 'wgtz'
write(21,'(a3)') 'dsw'
write(21,'(2e12.4)') infil, infil

write(21,'(a)') 'model 2  #canyon bottom, zone 102'
write(21,'(a2)') 'ti'
write(21,'(a)') '2 0 1e20'
write(21,'(a4)') 'wgtz'
write(21,'(a3)') 'dsw'
write(21,'(2e12.4)') canyon_flux, canyon_flux

write(21,'(a)') 'model 3   #kz = 0 at the bottom'
write(21,'(a2)') 'ti'
write(21,'(a)') '2 0 1e20'
write(21,'(a2)') 'kz'
write(21,'(2f8.2)') -20.0, -20.0

write(21,'(a)') 'model 4  # flux on west pw zone boundary nodes'
write(21,'(a2)') 'ti'
write(21,'(a)') '2 0 1e20'
write(21,'(a3)') 'dsw'
write(21,'(2e12.4)') west_flux, west_flux

write(21,'(a)') 'model 5   #water input in cdv canyon from outfall 260)'
write(21,'(a2)') 'ti'
write(21,'(a)') '2 0 1e20'
write(21,'(a3)') 'dsw'
write(21,'(70e14.5)') cdv_flux, cdv_flux

write(21,'(a3)') 'end'

write(21,'(a)') '-1    0   0  1'
write(21,'(a)') '-102  0   0  2'
write(21,'(a)') '-2    0   0  3'
write(21,'(a)') '-13   0   0  4'
write(21,'(a)') '-101  0   0  5'
write(21,*)

end
