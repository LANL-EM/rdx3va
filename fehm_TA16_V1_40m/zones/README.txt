WC15c / TA-16 zone names and numbers

28 "Qbof_G3"
27 "Qbof_G2"
26 "Qbt"
25 "Qbt4"
24 "Qbt3t"
23 "Qbt3"
22 "Qbt2"
21 "Qbt1vu"
20 "Qbt1vc"
19 "Qbt1g"
18 "Qbtt"
17 "Qct"
16 "Qbof"
15 "Qbog"
14 "Tpf3"
13 "Tb4"
12 "Qta"
11 "Tvt2"
10 "Tpf2"


Tet mesh has WC15c/TA-16 zones except for modified sparse units.
These units have single nodes scattered across mesh to coarse to capture the unit.
So these scattered nodes are merged with neighboring same material.

Merge 26 with 25
Merge 20 with 21
Merge 18 with 19
Merge 15 with 27

ZONE MATERIALS 

tet_ev_material.zone

Final Tet mesh zone materials:
dump/zone_imt/tet_ev/motet                                                      
*********dump_material_lists********                                            
Minimum material ID value =     10                                              
Maximum material ID value =     28                                              
Total possible materials  =     19                                              
Material          10 has      8575 nodes. #nodes/nnodes is   0.141698062420     
Material          11 has       144 nodes. #nodes/nnodes is   0.237953593023E-02 
No nodes found in material  =     12                                            
No nodes found in material  =     13                                            
Material          14 has     26705 nodes. #nodes/nnodes is   0.441288262606     
No nodes found in material  =     15                                            
Material          16 has      7380 nodes. #nodes/nnodes is   0.121951222420     
Material          17 has      1818 nodes. #nodes/nnodes is   0.300416424870E-01 
No nodes found in material  =     18                                            
Material          19 has       813 nodes. #nodes/nnodes is   0.134344631806E-01 
No nodes found in material  =     20                                            
Material          21 has       650 nodes. #nodes/nnodes is   0.107409609482E-01 
Material          22 has      1747 nodes. #nodes/nnodes is   0.288683976978E-01 
Material          23 has      2061 nodes. #nodes/nnodes is   0.340571105480E-01 
Material          24 has      1006 nodes. #nodes/nnodes is   0.166237019002E-01 
Material          25 has       761 nodes. #nodes/nnodes is   0.125751867890E-01 
No nodes found in material  =     26                                            
Material          27 has      4418 nodes. #nodes/nnodes is   0.730054825544E-01 
Material          28 has      4438 nodes. #nodes/nnodes is   0.733359754086E-01 


Interpolated Hex mesh colored from EV WC15c / TA-16.seq
Minimum material ID value =     10
Maximum material ID value =     28
Total possible materials  =     19
Material          10 has      8575 nodes. #nodes/nnodes is   0.141698062420
Material          11 has       144 nodes. #nodes/nnodes is   0.237953593023E-02
No nodes found in material  =     12
No nodes found in material  =     13
Material          14 has     26705 nodes. #nodes/nnodes is   0.441288262606
Material          15 has       204 nodes. #nodes/nnodes is   0.337100937031E-02
Material          16 has      7380 nodes. #nodes/nnodes is   0.121951222420
Material          17 has      1818 nodes. #nodes/nnodes is   0.300416424870E-01
Material          18 has        38 nodes. #nodes/nnodes is   0.627933128271E-03
Material          19 has       775 nodes. #nodes/nnodes is   0.128065301105E-01
Material          20 has         6 nodes. #nodes/nnodes is   0.991473352769E-04
Material          21 has       644 nodes. #nodes/nnodes is   0.106418132782E-01
Material          22 has      1747 nodes. #nodes/nnodes is   0.288683976978E-01
Material          23 has      2061 nodes. #nodes/nnodes is   0.340571105480E-01
Material          24 has      1006 nodes. #nodes/nnodes is   0.166237019002E-01
Material          25 has       706 nodes. #nodes/nnodes is   0.116663360968E-01
Material          26 has        55 nodes. #nodes/nnodes is   0.908850575797E-03
Material          27 has      4214 nodes. #nodes/nnodes is   0.696344748139E-01
Material          28 has      4438 nodes. #nodes/nnodes is   0.733359754086E-01

ZONE OUTSIDE

out_top.zonn
out_bottom.zonn
out_east_right.zonn
out_north_back.zonn
out_north_east.zonn
out_south_front.zonn
out_west_left.zonn

1 THE PSET  top     HAS       1476 POINTS                                           
2 THE PSET  bottom  HAS       1476 POINTS                                        
3 THE PSET  west    HAS        738 POINTS                                          
4 THE PSET  south   HAS       3485 POINTS                                         
5 THE PSET  east    HAS        287 POINTS                                          
6 THE PSET  north   HAS       2501 POINTS                                         
7 THE PSET  north_east  HAS   1476 POINTS                                    





