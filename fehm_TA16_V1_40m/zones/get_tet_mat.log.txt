read avs hex_ev_mat.inp mohex                                                   
read tet.inp motet                                                              
cmo setatt motet ipolydat no                                                    
resetpts itp                                                                    
cmo/setatt/motet imt 50                                                         
cmo/setatt/motet itetclr 50                                                     
cmo printatt mohex -xyz- minmax                                                 
cmo printatt motet -xyz- minmax                                                 
cmo printatt mohex imt1 minmax                                                  
cmo printatt motet imt1 minmax                                                  
cmo/select/motet                                                                
interpolate/voronoi/motet imt/1,0,0/mohex imt                                   
interpolate/map/motet itetclr/1,0,0/mohex itetclr                               
cmo printatt motet imt minmax                                                   
cmo printatt motet itetclr minmax                                               
dump gmv tet_ev_raw.gmv motet                                                   
dump/zone_imt/tet_ev_raw/motet                                                  
pset/p26/attribute imt/1,0,0/eq 26                                              
cmo/setatt/motet/imt/pset,get,p26/25                                            
eltset/e26/itetclr eq 26                                                        
cmo/setatt/motet/itetclr/eltset,get,e26/25                                      
pset/p20/attribute imt/1,0,0/eq 20                                              
cmo/setatt/motet/imt/pset,get,p20/21                                            
eltset/e20/itetclr eq 20                                                        
cmo/setatt/motet/itetclr/eltset,get,e20/21                                      
pset/p18/attribute imt/1,0,0/eq 18                                              
cmo/setatt/motet/imt/pset,get,p18/19                                            
eltset/e18/itetclr eq 18                                                        
cmo/setatt/motet/itetclr/eltset,get,e18/19                                      
pset/p15/attribute imt/1,0,0/eq 15                                              
cmo/setatt/motet/imt/pset,get,p15/27                                            
eltset/e15/itetclr eq 15                                                        
cmo/setatt/motet/itetclr/eltset,get,e15/27                                      
cmo printatt motet -all- minmax                                                 
cmo/DELATT/motet ccoef                                                          
cmo/DELATT/motet vorvol                                                         
cmo/DELATT/motet ij_ccoef                                                       
cmo/DELATT/motet imt_old                                                        
cmo/DELATT/motet imtraw                                                         
quality                                                                         
dump gmv tet_ev_mat.gmv motet                                                   
dump avs tet_ev_mat.inp motet                                                   
read avs wells_lines_all.inp mow                                                
cmo setatt mow imt 1                                                            
cmo setatt mow itetclr 1                                                        
addmesh merge moall motet mow                                                   
cmo setatt moall ipolydat no                                                    
dump gmv view_tet_mat_wells.gmv moall                                           
dump/zone_imt/tet_ev/motet                                                      
cmo printatt motet -all- minmax                                                 
finish                                                                          
