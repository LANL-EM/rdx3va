 
 
*               * * * * * * * * * * * * * * * * * * * * * * * *                 
*               *                                             *                 
*               *    Program:  LaGriT V3.107   Linux m64      *                 
*               *    date_compile: 2015/06/24  Ubu gf.so      *                 
*               *    Run Time: 2016/Mar  3  08:27:50          *                 
*               *    Manual:   http://lagrit.lanl.gov         *                 
*               *                                             *                 
*               * * * * * * * * * * * * * * * * * * * * * * * *                 
 
                               -----oOo-----                                    
                           LaGriT V3 LACC-2012-084                              
LaGriT Copyright: This program was prepared by Los Alamos National Security, LLC
at Los Alamos National Laboratory (LANL) under contract No. DE-AC52-06NA25396   
with the U.S. Department of Energy (DOE). All rights in the program are reserved
by the DOE and Los Alamos National Security, LLC. Permission is granted to the  
public to copy and use this software without charge, provided that this Notice  
and any statement of authorship are reproduced on all copies. Neither the       
U.S. Government nor LANS makes any warranty, express or implied, or assumes     
any liability or responsibility for the use of this software.                   
                               -----oOo-----                                    
 
 
Output log file: outx3dgen                                                      
Command log file: logx3dgen                                                     
 
# UPZ1 = upper perched zone 1                                                   
# also known as smaller upper perched 2                                         
#                                                                               
# nodes below upper perched bottom                                              
# and nodes inside perched volume                                               
# assign perched wtr id in the 60's                                             
read avs tet_ev_mat.inp cmotet                                                  
geniee                                                                          
finish                                                                          
cmo/status/brief                                                                
 
The current-mesh-object(CMO) is: cmotet                                         
 
  1 Mesh Object name: cmotet                                                    
    number of nodes =         60516        number of elements =       345050    
    dimensions geometry =         3        element type =                tet    
    dimensions topology =         3        4 nodes      4 faces      6 edges    
    boundary flag =        16000000        status =                   active    
 
 
finish                                                                          
resetpts itp                                                                    
geniee                                                                          
finish                                                                          
cmo setatt cmotet ipolydat no                                                   
         1 values reset for attribute ipolydat                                  
 
cmo/addatt/cmotet/wtrpts/VINT/scalar/nnodes/linear/permanent/afgx/0             
cmo/addatt/cmotet/wtredge/VINT/scalar/nnodes/linear/permanent/afgx/0            
cmo printatt cmotet -all- minmax                                                
ATTRIBUTE NAME              MIN               MAX         DIFFERENCE    LENGTH  
 -def-              0.000000000E+00  0.000000000E+00 0.000000000E+00     60516  
 scalar                           1                1               0         1  
 vector                           3                3               0         1  
 nnodes                       60516            60516               0         1  
 nedges                           0                0               0         1  
 nfaces                           0                0               0         1  
 nelements                   345050           345050               0         1  
 mbndry                    16000000         16000000               0         1  
 ndimensions_topo                 3                3               0         1  
 ndimensions_geom                 3                3               0         1  
 nodes_per_element                4                4               0         1  
 edges_per_element                6                6               0         1  
 faces_per_element                4                4               0         1  
 isetwd                           0                0               0     60516  
 ialias                           0                0               0     60516  
 imt1                            10               28              18     60516  
 itp1                             0               12              12     60516  
 icr1                             0                0               0     60516  
 isn1                             0                0               0     60516  
 xic                4.906650000E+05  4.937850000E+05 3.120000000E+03     60516  
 yic                5.374450000E+05  5.383650000E+05 9.200000000E+02     60516  
 zic                1.700000000E+03  2.347826246E+03 6.478262465E+02     60516  
 xtetwd                           0                0               0    345050  
 itetclr                         10               28              18    345050  
 itettyp                          5                5               0    345050  
 itetoff                          0          1380196         1380196    345050  
 jtetoff                          0          1380196         1380196    345050  
 itet                             1            60516           60515    345050x4
 jtet                             1         17380200        17380199    345050x4
 epsilon            1.000000004E-15  1.000000004E-15 0.000000000E+00         1  
 epsilonl           7.364546702E-10  7.364546702E-10 0.000000000E+00         1  
 epsilona           2.442596979E-06  2.442596979E-06 0.000000000E+00         1  
 epsilonv           4.128964854E-04  4.128964854E-04 0.000000000E+00         1  
 ipointi                          1                1               0         1  
 ipointj                      60516            60516               0         1  
 idebug                           0                0               0         1  
 itypconv_sm                      1                1               0         1  
 maxiter_sm                      25               25               0         1  
 tolconv_sm         1.000000000E+00  1.000000000E+00 0.000000000E+00         1  
 nnfreq                           1                1               0         1  
 ivoronoi                         1                1               0         1  
 iopt2to2                         2                2               0         1  
 xmin               4.906650000E+05  4.906650000E+05 0.000000000E+00         1  
 ymin               5.374450000E+05  5.374450000E+05 0.000000000E+00         1  
 zmin               1.700000000E+03  1.700000000E+03 0.000000000E+00         1  
 xmax               4.937850000E+05  4.937850000E+05 0.000000000E+00         1  
 ymax               5.383650000E+05  5.383650000E+05 0.000000000E+00         1  
 zmax               2.347826246E+03  2.347826246E+03 0.000000000E+00         1  
 kdtree_level                     0                0               0         1  
 max_number_sets                 64               64               0         1  
 number_of_psets                  0                0               0         1  
 number_of_eltsets                0                0               0         1  
 number_of_fsets                  0                0               0         1  
 wtrpts                           0                0               0     60516  
 wtredge                          0                0               0     60516  
 
read avs surf_upper2_bottom.inp cmobot                                          
cmo/addatt/cmobot/x_meters/VDOUBLE/scalar/nnodes/linear/permanent/gxaf/0.0      
finish                                                                          
cmo/addatt/cmobot/y_meters/VDOUBLE/scalar/nnodes/linear/permanent/gxaf/0.0      
finish                                                                          
cmo/addatt/cmobot/z_meters/VDOUBLE/scalar/nnodes/linear/permanent/gxaf/0.0      
finish                                                                          
cmo/addatt/cmobot/x_feet/VDOUBLE/scalar/nnodes/linear/permanent/gxaf/0.0        
finish                                                                          
cmo/addatt/cmobot/y_feet/VDOUBLE/scalar/nnodes/linear/permanent/gxaf/0.0        
finish                                                                          
cmo/addatt/cmobot/z_feet/VDOUBLE/scalar/nnodes/linear/permanent/gxaf/0.0        
finish                                                                          
geniee                                                                          
finish                                                                          
cmo/status/brief                                                                
 
The current-mesh-object(CMO) is: cmobot                                         
 
  1 Mesh Object name: cmotet                                                    
    number of nodes =         60516        number of elements =       345050    
    dimensions geometry =         3        element type =                tet    
    dimensions topology =         3        4 nodes      4 faces      6 edges    
    boundary flag =        16000000        status =                 inactive    
 
  2 Mesh Object name: cmobot                                                    
    number of nodes =         28839        number of elements =        56915    
    dimensions geometry =         3        element type =                tri    
    dimensions topology =         2        3 nodes      3 faces      3 edges    
    boundary flag =        16000000        status =                   active    
 
 
finish                                                                          
read avs surf_upper2_top.inp cmotop                                             
cmo/addatt/cmotop/x_meters/VDOUBLE/scalar/nnodes/linear/permanent/gxaf/0.0      
finish                                                                          
cmo/addatt/cmotop/y_meters/VDOUBLE/scalar/nnodes/linear/permanent/gxaf/0.0      
finish                                                                          
cmo/addatt/cmotop/z_meters/VDOUBLE/scalar/nnodes/linear/permanent/gxaf/0.0      
finish                                                                          
cmo/addatt/cmotop/x_feet/VDOUBLE/scalar/nnodes/linear/permanent/gxaf/0.0        
finish                                                                          
cmo/addatt/cmotop/y_feet/VDOUBLE/scalar/nnodes/linear/permanent/gxaf/0.0        
finish                                                                          
cmo/addatt/cmotop/z_feet/VDOUBLE/scalar/nnodes/linear/permanent/gxaf/0.0        
finish                                                                          
geniee                                                                          
finish                                                                          
cmo/status/brief                                                                
 
The current-mesh-object(CMO) is: cmotop                                         
 
  1 Mesh Object name: cmotet                                                    
    number of nodes =         60516        number of elements =       345050    
    dimensions geometry =         3        element type =                tet    
    dimensions topology =         3        4 nodes      4 faces      6 edges    
    boundary flag =        16000000        status =                 inactive    
 
  2 Mesh Object name: cmobot                                                    
    number of nodes =         28839        number of elements =        56915    
    dimensions geometry =         3        element type =                tri    
    dimensions topology =         2        3 nodes      3 faces      3 edges    
    boundary flag =        16000000        status =                 inactive    
 
  3 Mesh Object name: cmotop                                                    
    number of nodes =         28839        number of elements =        56915    
    dimensions geometry =         3        element type =                tri    
    dimensions topology =         2        3 nodes      3 faces      3 edges    
    boundary flag =        16000000        status =                   active    
 
 
finish                                                                          
cmo/select/cmotet                                                               
surface/surfbot/intrface/sheet/cmobot                                           
cmo/addatt//v2/INT/scalar/scalar/constant/permanent//2.0                        
finish                                                                          
cmo/addatt//linkt/VINT/v2/nelements//permanent/x/0.0                            
finish                                                                          
cmo/addatt//v12/INT/scalar/scalar/constant/permanent//12.0                      
finish                                                                          
cmo/addatt//sbox/VDOUBLE/v12/nelements/linear/permanent/x/0.0                   
finish                                                                          
log/tty/off                                                                     
finish                                                                          
cmo/DELATT/surfbot isetwd                                                       
finish                                                                          
cmo/DELATT/surfbot ialias                                                       
finish                                                                          
cmo/DELATT/surfbot imt1                                                         
finish                                                                          
cmo/DELATT/surfbot itp1                                                         
finish                                                                          
cmo/DELATT/surfbot icr1                                                         
finish                                                                          
cmo/DELATT/surfbot isn1                                                         
finish                                                                          
cmo/DELATT/surfbot xtetwd                                                       
finish                                                                          
cmo/DELATT/surfbot itetclr                                                      
finish                                                                          
log/tty/on                                                                      
finish                                                                          
surface/surftop/intrface/sheet/cmotop                                           
cmo/addatt//v2/INT/scalar/scalar/constant/permanent//2.0                        
finish                                                                          
cmo/addatt//linkt/VINT/v2/nelements//permanent/x/0.0                            
finish                                                                          
cmo/addatt//v12/INT/scalar/scalar/constant/permanent//12.0                      
finish                                                                          
cmo/addatt//sbox/VDOUBLE/v12/nelements/linear/permanent/x/0.0                   
finish                                                                          
log/tty/off                                                                     
finish                                                                          
cmo/DELATT/surftop isetwd                                                       
finish                                                                          
cmo/DELATT/surftop ialias                                                       
finish                                                                          
cmo/DELATT/surftop imt1                                                         
finish                                                                          
cmo/DELATT/surftop itp1                                                         
finish                                                                          
cmo/DELATT/surftop icr1                                                         
finish                                                                          
cmo/DELATT/surftop isn1                                                         
finish                                                                          
cmo/DELATT/surftop xtetwd                                                       
finish                                                                          
cmo/DELATT/surftop itetclr                                                      
finish                                                                          
log/tty/on                                                                      
finish                                                                          
region/r_below/lt surfbot                                                       
region/r_inside/ge surfbot and le surftop                                       
pset UPZ1_blw/region r_below/1,0,0                                              
 
 THE PSET  UPZ1_blw  HAS      33543 POINTS                                      
pset/UPZ1_in/region r_inside                                                    
 
 THE PSET  UPZ1_in  HAS       8195 POINTS                                       
# let inside win over below if intersect                                        
cmo setatt cmotet/wtrpts/0                                                      
     60516 values reset for attribute wtrpts                                    
 
cmo setatt cmotet/wtrpts/pset,get,UPZ1_blw 1                                    
     33543 values reset for attribute wtrpts                                    
 
cmo setatt cmotet/wtrpts/pset,get,UPZ1_in 2                                     
      8195 values reset for attribute wtrpts                                    
 
dump gmv tet_perched_wtr.gmv cmotet                                             
cmo/modatt/-def-/-def-/ioflag/x                                                 
finish                                                                          
# WRITE zonn files                                                              
pset/UPZ1_blw/zonn/wtr_UPZ1_blw/ascii/60                                        
PSET: THERE ARE  2 PSETS DEFINED                                                
  UPZ1_blw          UPZ1_in                                                     
PSET: Appended .zonn to the file name                                           
PSET: OUTPUT UPZ1_blw    1 PSETS TO FILE                                        
pset/UPZ1_in/zonn/wtr_UPZ1_in/ascii/61                                          
PSET: THERE ARE  2 PSETS DEFINED                                                
  UPZ1_blw          UPZ1_in                                                     
PSET: Appended .zonn to the file name                                           
PSET: OUTPUT UPZ1_in    1 PSETS TO FILE                                         
# capture interface for viewing                                                 
eltset/e1/inclusive pset,get,UPZ1_in                                            
 
 THE ELTSET e1                               HAS      59827 ELEMENTS            
eltset/e2/inclusive pset,get,UPZ1_blw                                           
 
 THE ELTSET e2                               HAS     195808 ELEMENTS            
pset/p1/eltset e1                                                               
 
 THE PSET  p1  HAS      11901 POINTS                                            
pset/p2/eltset e2                                                               
 
 THE PSET  p2  HAS      35682 POINTS                                            
pset/pboth inter p1 p2                                                          
 
 THE PSET  pboth  HAS       4018 POINTS                                         
cmo setatt cmotet wtredge pset,get,pboth 1                                      
      4018 values reset for attribute wtredge                                   
 
pset/pnot/attribute wtrpts/pset,get,pboth/eq 0                                  
 
 THE PSET  pnot  HAS        146 POINTS                                          
cmo setatt cmotet wtredge pset,get,pnot 0                                       
       146 values reset for attribute wtredge                                   
 
dump gmv tet_perched_wtr_upz1.gmv cmotet                                        
cmo/modatt/-def-/-def-/ioflag/x                                                 
finish                                                                          
finish                                                                          
LaGriT successfully completed                                                   
