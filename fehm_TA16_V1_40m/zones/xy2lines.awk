# this converts a set of xy values to avs lines
# with constant elevation
BEGIN{ idx=0; elev= 0.00; }
{
if (NF == 2 && $1 != "#") {
     idx=idx+1
     xic[idx]= $1
     yic[idx]= $2
}

} END { 
  FS=" "
  print idx, idx-1, " 0 0 0";
  for (i=1; i<=idx; i=i+1) {

   printf(" %5d %14.4f  %14.4f  %7.4f\n",i, xic[i], yic[i], elev ) } 
#   print i, xic[i], yic[i], elev }
 
  for (i = 1; i < (idx); i = i + 1){  print i, "1 line ",i,i+1; } 
}

