info("start")
include(joinpath(Pkg.dir("Mads"), "src-interactive/MadsParallel.jl"))
info("setprocs")
setprocs(ntasks_per_node=8)

info("import")
reload("Mads")
@everywhere Mads.quietoff()
info("set")

info("load")
md = Mads.loadmadsfile("ta16.mads")
Mads.addkeyword!(md, "respect_space")
# info("forward")
# Mads.forward(md)
# @show ReusableFunctions.restart
#info("local SA")
#lsa_results = Mads.localsa(md, datafiles=true, imagefiles=true)
@show ReusableFunctions.restarts
# JLD.save("lsa_results.jld", "lsa_results", lsa_results)
info("calibrate")
inverse_parameters, inverse_results = Mads.calibrate(md; np_lambda=24, lambda_mu=1.2, tolX=1e-14)
@show ReusableFunctions.restarts
Mads.setparamsinit!(md, inverse_parameters)
Mads.savemadsfile(md)
