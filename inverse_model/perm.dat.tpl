template |
perm
-10     0       0     3.61000000E-12    3.61000000E-12   3.61000000E-12 #       Tpf2
-11     0       0     2.96000000E-13    2.96000000E-13   2.96000000E-13 #       Tvt2    Younger Tschicoma
-14	0	0     |   kxy_14  |    |   kxy_14  |   |     kz_14  |	#	Tpf3
-16	0	0     |   kxy_16  |    |   kxy_16  |   |     kz_16  |      #	Qbof	Otowi	Member,	ash	flow	
-17	0	0     |   kxy_17  |    |   kxy_17  |   |     kz_17  |      #	Qct	Cerro	Toledo			
-19	0	0     |   kxy_19  |    |   kxy_19  |   |     kz_19  |      #	Qbt1g	Tshirege	Unit	1	-	glassy	
-21	0	0     |   kxy_21  |    |   kxy_21  |   |     kz_21  |      #	Qbt1vu	Tshirege	Unit	1	-	vitric	
-22	0	0     |   kxy_22  |    |   kxy_22  |   |     kz_22  |      #	Qbt2	Tshirege	Unit	2		
-23	0	0     |   kxy_23  |    |   kxy_23  |   |     kz_23  |      #	Qbt3	Tshirege	Unit	3		
-24	0	0     |   kxy_24  |    |   kxy_24  |   |     kz_24  |      #	Qbt3t	Tshirege	Unit	3,	transition	zone	
-25	0	0     |   kxy_25  |    |   kxy_25  |   |     kz_25  |      #	Qbt4	Tshirege	Unit	4		
-27	0	0     |   kxy_27  |    |   kxy_27  |   |     kz_27  |      #	Qbof	G2	Otowi	Member,	ash	flow
-28	0	0     |   kxy_28  |    |   kxy_28  |   |     kz_28  |      #	Qbof	G3	Otowi	Member,	ash	flow
-140	0	0     |   kxy_140  |   |   kxy_140  |  |     kz_140  |     #	Tpf3 in perched zone
-141	0	0     |   kxy_141  |   |   kxy_141  |  |     kz_141  |     #	Tpf3 in perched zone
-142	0	0     |   kxy_142  |   |   kxy_142  |  |     kz_142  |     #	Tpf3 in perched zone
 
