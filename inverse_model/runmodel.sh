#!/usr/bin/env bash

rm -f *.avs
rm -f flow*.mat_head flow*.vec_head flow*.sca_head flow*.chk flow*.fin flow*.outp flow*.avs_log simu_thickness_* simu_thickness.* thickness
rm -f tran.mat_head tran.vec_head tran.sca_head tran.chk tran.fin tran.outp tran.avs_log
rm -f fehmn.err boun.macro boun_st.macro
../codes/write_boun.x
/turquoise/users/vvv/bin/xfehm flow0.files
/turquoise/users/vvv/bin/xfehm flow.files
/turquoise/users/vvv/bin/xfehm trans.files
../codes/post_process_avs_getpwz.x
cat ./simu_thickness.dat | awk '{print $3}' > thickness
../codes/post_process_conc.x
