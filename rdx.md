Data Package: LANL site model `RDX`
==========================================

The model `RDX` is designed to represent the groundwater flow and contaminant transport in the vadose zone and regional aquifer at the LANL RDX contamination site (near TA-16 area).
The model parameters are estimated by calibration against observed field data.
This inverse analyses are carried out using [Mads](http://mads.lanl.gov).
The modeling involves a number of forward model runs, which is accomplished using [FEHM](http://fehm.lanl.gov)

The model domain includes both saturated and unsaturated zones.
In the vadose zone, van Genuchten water-retention model is used to describe the constitutive relationship between the water pressure and water content.
An extension of the van Genuchten model based on Mualem-Mualem theory is applied to the describe the constitutive relationship between the water content and relative unsaturated permeability.

`.....`

The purpose of this study is to build up a calibrated model to reproduce observed shapes and locations of the perched zones, and measured transient RDX concentrations at various locations at the LANL site.

This model is released under GPL3 license.

Computational grid
---
3D model domain and computational grid of the RDX model:

![grid](./figures/tet_a.png "3D grid")

Detailed description of the computational grid can be found from [README](https://meshing.lanl.gov/proj/ER_LANL_TA-16/images_V1_40m/README.txt) or [here](https://meshing.lanl.gov/proj/ER_LANL_TA-16/index.html), and the spatial distribution of stratigraphic units can be found [here](https://meshing.lanl.gov/proj/ER_LANL_TA-16/images_V1_40m/gallery.html).

Q: can you please move the files above in the repo?


Model setup
-----------
The goal is to build up a model that honors the observed perched zones and measured RDX concentrations at various locations.
The initial and boundary conditions for the flow simulations are prescribed as follows:
- the entire perched zones are initially assigned to constant hydrostatic pressure head and full saturation;
- water table locations are fixed by assigning constant hydrostatic pressure head and saturation of 1 to all nodes in the saturated zone;
- the boundary nodes that are part of perched water zones are assigned to constant hydrostatic pressure head and full saturation;
- permeability reduction was applied to the bottom of the perched zones;
- the water source from 260 outfall was mainly distributed to CdV canyon (99%) and the rest (1%) to the pond;

For transport simulations, the following additional assumptions were made:
- zero initial RDX concentration in the entire domain at the beginning of the simulation (1/1/1950)
- RDX concentration at the 260 outfall is set to be constant and equal to 44 mg/l (the solubility limit)
- the flow rate at 260 outfall is defined based on Method 4b of xxx[]

Model parameters to be estimated
--------------------------------
The model parameters to be estimated include:

- anisotropic permeability values for 14 hydro-stratigraphic units;
- permeability reduction factors along the bottoms of the perched zones;
- infiltration rates at the ground surface;
- canyon infiltration flux (Q: is it different from one below);
- CdV canyon infiltration flux (not including the flux from 260 outfall, which is a known time series);
- water influx from western boundary (mountain-front recharge);
- sorption coefficient (Kd);
- matrix diffusion coefficient;
- dispersivity in three directions.

Measurements available
---------------------

- thickness of upper/lower perched zones at 2952 locations (based on model developed by Broxton et al., 2016)
- 802 RDX concentration measurements from xx well/screens representative for transients observed between 1997 and 2017 (Q: correct?).

Model execution
--------------
The model simulations were performed on wolf, a LANL high-performance computing platform.
An inverse model job can be submitted to the queue using the following command line:

```bash
sbatch batch
```

where _bacth_ is a script file that specifies, among other information, the number of computational nodes, the number of hours, as well as the executables for the job.

Example model outputs
---------------------
The performance of the inverse model can be evaluated by comparing simulated and observed (1) perched zone thickness and (2) RDX concentration.
The following figure illustrates the simulated perched zones.

![](./figures/perchedZone2.png)
This following figure compares the observed and simulated thickness of upper perched zone.

![](./figures/calibrated_upz2.png)

The comparisons of the observed and simulated RDX concentration can be found in this [excel file](https://gitlab.com/lanl/rdx3va/blob/master/figures/comp_RDX_conc.xlsx).

Directory structure:
-------

All the files associated with the RDX model are listed as follows:

```
   |-data/
   |---obs_nodes.dat
   |---obs_time_conc.dat
   |---obs_zonn_nodes
   |---outfall260.dat
   |---overlapping_source_obs_nodes
   |---post_process_avs_getpwz.input
   |---rdx_obs.zonn
   |---rlp.dat
   |---rock.dat
   |---surf_obs_locations.dat
   |---time.macro
   |---true_thickness_1.dat
   |---true_thickness_2.dat
   |---userc_data.dat
   |---well_obs_locations.dat
   |-grid/
   |---tet.fehmn
   |---tet.geo
   |---tet.stor
   |---tet_ev_material.zone
   |---canyon_bot.zonn
   |---canyon_cdv.zonn
   |---out_bottom.zonn
   |---out_east_right.zonn
   |---out_north_back.zonn
   |---out_north_east.zonn
   |---out_south_front.zonn
   |---out_top.zonn
   |---out_west_left.zonn
   |---wtr_abv.zone
   |---wtr_abv.zonn
   |---wtr_blw_ne.zonn
   |---wtr_blw_north.zonn
   |---wtr_blw_south.zonn
   |---wtr_blw_west.zonn
   |---wtr_blw.zone
   |---wtr_blw.zonn
   |---wtr_LPZ1_blw.zonn
   |---wtr_LPZ1_in.zonn
   |---wtr_UPZ1_blw.zonn
   |---wtr_UPZ1_in.zonn
   |---wtr_UPZ2_blw_addNW2.zonn
   |---wtr_UPZ2_blw_addNW.zonn
   |---wtr_UPZ2_blw.zonn
   |---wtr_UPZ2_in_addNW2.zonn
   |---wtr_UPZ2_in_addNW.zonn
   |---wtr_UPZ2_in.zonn
   |---wtr_UPZ2_Tpf3s_intr.zonn
   |---wtr_UPZ2_Tpf3s_win.zonn
   |---wtr_UPZ2_Tpf3s.zonn
   |---setBoundConditions/
   |------pres_lpz1.macro
   |------pwZone_presFlow_macro.input
   |------satZone_presFlow_macro.input
   |---setBoundConditions_addNW/
   |------pres_sat.macro
   |------flow_sat.macro
   |------satZone_presFlow_macro.input
   |------pwZone_presFlow_macro.input
   |---setBoundConditions_addNW2/
   |------pres_UPZ2.macro
   |------flow_UPZ2_allsides.macro
   |------UPZ2_allsides.zonn
   |------pwZone_presFlow_macro.input
   |------satZone_presFlow_macro.input
   |-codes/
   |---get_source2obs_dist.x
   |---post_process_avs_getpwz.x
   |---post_process_conc.x
   |---satZone_presFlow_macro.x
   |---write_presFlow_macro.x
   |---write_presFlow_macro_2.x
   |---write_boun.x
   |-setupBoundaries/
   |---setBoundConditions/
   |---setBoundConditions_addNW/
   |---setBoundConditions_addNW2/
   |-initialRun4PE/
   |---flow.fin
   |---boun.macro
   |---flow0.data
   |---flow.data
   |---flow0.files
   |---flow.files
   |---boun_st.macro
   |---perm.dat
   |-inverse_model/
   |---calibrate-wtrw.jl
   |---ta16.mads
   |---perm.dat.tpl
   |---flow0.data.tpl
   |---flow.data.tpl
   |---trans.data.tpl
   |---trac.macro.tpl
   |---boun.para.tpl
   |---runmodel.sh
   |---flow0.files
   |---flow.files
   |---trans.files
   |---

```

- `grid` directory contains mesh files, and various zone files.
- `data` directory contains information about the site in various files.
- `codes` directory contains several FORTRAN codes for post-processing the flow field and concentration output.
- `setupBoundaries` directory contains several subdirectories for determining boundary nodes and creating `flow` macro files.
- `initianlRun4PE` directory contains an initial forward model run with initial model parameters; this `fin` file from this model run will be used as an initial guess for all forward runs in the inverse model.

